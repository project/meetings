
INTRODUCTION
------------
Meetings provide a content type, views and permissions to allow users to create
and manage meetings. Features include: date/time, location, purpose, agenda, 
notes, attachments

You can manage field-level permissions to allow/disallow access to notes and agendas.



INSTALLATION
------------
* Copy meetings directory to your modules 
  directory (usually <root>/sites/all/modules/)
* Enable the Meetings feature at <root>/admin/build/features 
* Set permissions 
 * create/edit own/edit all meetings, view/edit each field of meetings


ADVANCED
--------
If you install the node_repeat module, you will automatically be able to
schedule repeated meetings.


test change