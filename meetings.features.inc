<?php

/**
 * Implementation of hook_content_default_fields().
 */
function meetings_content_default_fields() {
  module_load_include('inc', 'meetings', 'meetings.defaults');
  $args = func_get_args();
  return call_user_func_array('_meetings_content_default_fields', $args);
}

/**
 * Implementation of hook_node_info().
 */
function meetings_node_info() {
  module_load_include('inc', 'meetings', 'meetings.features.node');
  $args = func_get_args();
  return call_user_func_array('_meetings_node_info', $args);
}

/**
 * Implementation of hook_user_default_permissions().
 */
function meetings_user_default_permissions() {
  module_load_include('inc', 'meetings', 'meetings.defaults');
  $args = func_get_args();
  return call_user_func_array('_meetings_user_default_permissions', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function meetings_views_default_views() {
  module_load_include('inc', 'meetings', 'meetings.features.views');
  $args = func_get_args();
  return call_user_func_array('_meetings_views_default_views', $args);
}
